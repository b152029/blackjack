/**
 * ブラックジャック問題6
 * (2016/08/17 内定者サマーキャンプ 問題解答例)
 *
 * ・ランダムで1～13(A～K)の数字をディーラーとプレイヤーに２枚ずつ配る。
 * ・勝敗の判定を行う。
 * ・プレイヤーに追加カードを引くかひかないかの機能を付ける。
 * ・勝敗判定を改良。
 *  バースト判定を付ける。22以上になったら負け。
 *  Aは、1または11どちらかを都合のいい方に解釈。
 *  どちらも21の場合、2枚でのブラックジャックが最強役となる。
 * ・ディーラーが追加カードを引くようにする。(17点以上になるまで)
 * ・カードにマークを付ける。
 *
 *
 */

import java.io.IOException;

public class BlackJack {

  PlayerPerson player;
  DealerPerson dealer;

  Deck deck;

  public static void main(String args[]) throws java.io.IOException {

    BlackJack bj = new BlackJack();
    bj.run();

  }

  public void run() throws IOException {
    // プレイヤー、ディーラーを作成
    player = new PlayerPerson();
    dealer = new DealerPerson();
    player.setName("Player");
    dealer.setName("Dealer");
    //デッキ作成、シャッフル
    deck = new Deck();
    deck.Shuffle();

    // デッキからランダムでカードをディーラーとプレイヤーに２枚ずつ配る
    player.AddCard(deck.Draw());
    player.AddCard(deck.Draw());
    dealer.AddCard(deck.Draw());
    dealer.AddCard(deck.Draw());

    //Dealerが１７点以上になるまでカードを引き続けるようにさせる
    dealer.CardAutoDraw(deck);
    //Playerにもう一度引くか確認
    player.CardDrawCheck(deck);

    // 勝敗判定
    System.out.println("*** 最終結果 ***");
    System.out.println("Playerの手札合計:" + player.getHandsSum());
    System.out.println("Playerの手札" + player.getHandsAll());
    System.out.println("Dealerの手札合計:" + dealer.getHandsSum());
    System.out.println("Delaerの手札" + dealer.getHandsAll());

    String winLoseResult = "勝者:";
    if (player.WinLoseDecision(dealer) != -1) {
      winLoseResult += player.getName();
    } else {
      winLoseResult += dealer.getName();
    }
    System.out.println(winLoseResult);

  }

}
