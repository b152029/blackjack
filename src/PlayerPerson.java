import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author b152029
 *
 */
public class PlayerPerson extends Person {

  /**
   * 追加でカードを引かせる処理
  * @param deck
  * @throws IOException
  */
  public void CardDrawCheck(Deck deck) throws IOException {

    // 追加でカードを引くか
    System.out.println("***追加でカードを引きますか？ y/n  ***");
    System.out.println(getHandsAll());
    System.out.println("Playerの手札合計:" + getHandsSum());

    // 1秒待つ
    waitForSeconds(1);

    String inputStr = null;
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    try {
      inputStr = br.readLine();
    } catch (IOException e) {
      System.out.println("入力エラー:" + e.getMessage());
    }

    if (inputStr.equals("y")) {
      //もう一枚引く
      AddCard(deck.Draw());

      // 22以上なら強制終了
      if (getHandsSum() >= 22) {
        System.out.println("!!!!!バーストしました!!!!!");
        System.out.println(getHandsAll());
      } else {
        //もう一度聞く
        CardDrawCheck(deck);
      }
    } else if (inputStr.equals("n")) {
      // 入力 - N - 110 なら終了

    } else {
      System.out.println("もう一度入力してください");
      //もう一度聞く
      CardDrawCheck(deck);
    }
  }

  /**
  * x秒待つ
  * @param time 1 - 1秒
  */
  public void waitForSeconds(int time) {
    try {
      Thread.sleep(time * 1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
