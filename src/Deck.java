import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * @author b152029
 *
 */
public class Deck {

  private ArrayList<Card> deck;

  /**
  * インスタンス化されたときにカードもセットする
  */
  public Deck() {
    deck = new ArrayList<Card>();
    DeckSetAll();
  }

  /**
  * カードをデッキにすべてセットする
  * H 1-13
  * D 1-13
  * S 1-13
  * C 1-13
  * 合計 52枚セット
  */
  public void DeckSetAll() {
    //AddCard(number,mark)
    for (int i = 0; i <= 3; i++) {
      for (int j = 1; j <= 13; j++) {
        AddCard(j, i);
      }
    }
  }

  /**
   * デッキの最後尾にカードを入れる
  * @param number
  * @param mark
  */
  public void AddCard(int number, int mark) {
    Card card = new Card(number, mark);
    deck.add(card);
  }

  /**
   * デッキの最後尾にカードを入れる
  * @param card
  */
  public void AddCard(Card card) {
    deck.add(card);
  }

  /**
  * 一枚引く
  * 1.デッキ内からCardをランダムに1枚取り出す
  * 2.取り出したCardをデッキ内から消去
  * 3.Cardを返す
  */
  public Card RandomDraw() {
    //ランダム数値作成 (0-デッキの最大数)
    Random rndGen = new Random();
    int rndNum = rndGen.nextInt(deck.size());

    //ランダムに1枚取り出し、デッキ内から消去
    Card card = deck.get(rndNum);
    deck.remove(rndNum);

    //Cardを返す
    return card;
  }

  /**
   * デッキの先頭からカードを1枚引く動作
   * 引いたカードはデッキから消す。
  * @return
  */
  public Card Draw() {
    Card card = deck.get(0);
    deck.remove(0);
    return card;
  }

  /**
  * デッキをシャッフルする
  * Card配列をシャッフル
  */
  public void Shuffle() {
    Collections.shuffle(deck);
  }

  public ArrayList<Card> getDeck() {
    return deck;
  }

  public void setDeck(ArrayList<Card> deck) {
    this.deck = deck;
  }

}
