import java.util.ArrayList;

/**
 * @author b152029
 *
 * 自分・相手の継承元クラス
 *
 * -自分の手札
 *
 * +手札にカードを入れる()
 * +手札の数値の合計を返す()
 * +手札の数を返す()
 * +
 */
public class Person {

  private ArrayList<Card> hands;
  private String personName;

  /**
   * プレイヤーの手札にカードを追加
  * @param card  Deckクラスに格納されているカードが入ってくる
  */
  public Person() {
    hands = new ArrayList<Card>();
  }

  public void AddCard(Card card) {
    this.hands.add(card);
  }

  /**
   * 手札の数値合計を返す。
   * ただし、Ａは1か11の都合の良い方にする。
   *
  * @return 手札のカードの数値合計
  */
  public int getHandsSum() {
    int handSum = 0;
    int aceCount = 0;

    //Aは1で計算
    for (Card card : hands) {
      //手札のＡをカウント
      if (card.getBlackJackPoint() == 1) {
        aceCount++;
      }
      handSum += card.getBlackJackPoint();
    }

    //Ａを都合の良い方に解釈する
    if (handSum <= 10 && aceCount >= 1) {
      handSum += 10;
    }

    return handSum;
  }

  /**
  * @return 手札の数
  */
  public int getHandsCount() {
    return this.hands.size();
  }

  /**自分の手札をすべて表示する
  * @return String
  */
  public String getHandsAll() {
    String result = "";
    for (Card card : hands) {
      result += card.toString();
    }

    return result;
  }

  /**
   * 相手の手札の合計値と比べて勝利したら1を返す
  * @param otherPerson
  * @return int 1   勝ち
  * @return int -1   負け
  */
  public int WinLoseDecision(Person otherPerson) {
    int playerHandsSum = this.getHandsSum();
    int otherHandsSum = otherPerson.getHandsSum();
    int result = 0;

    //バースト判定
    if (playerHandsSum >= 22) {
      return -1;
    } else if (otherHandsSum >= 22) {
      return 1;
    }

    //両方同じなら引き分け
    if (playerHandsSum == otherHandsSum) {
      return 0;
    }

    // 勝敗判定 21に近いほうが勝ち
    int[] playerHandSums = { playerHandsSum, otherHandsSum };
    int key = 21;
    // 近さを計算 絶対値を取る
    result = Math.abs(playerHandSums[0] - key) <= Math.abs(playerHandSums[1] - key) ? 1 : -1;

    return result;
  }

  public void setName(String personName) {
    this.personName = personName;
  }

  public String getName() {
    return personName;
  }

}
