/**
 * @author b152029
 *
 * カード1枚の構成要素
 * -数字
 * -模様
 *
 */
public class Card {

  int number;
  int mark;

  final char MARK_HEART = 'H';
  final char MARK_DIAMOND = 'D';
  final char MARK_SPADE = 'S';
  final char MARK_CLOVER = 'C';

  public Card() {
  }

  public Card(int number, int mark) {
    this.number = number;
    this.mark = mark;
  }

  public void setNumber(int number) {
    this.number = number;
  }

  /**
   * カードの点数をそのまま返す。J,Q,K = 11,12,13
  * @return
  */
  public int getNumber() {
    return number;
  }

  /**カードの点数をブラックジャック方式で返す。A = 1 JQK = 全て10
  * @return
  */
  public int getBlackJackPoint() {
    return number <= 10 ? number : 10;
  }

  public void setMark(int mark) {
    this.mark = mark;
  }

  public char getMark() {
    char markChar = 'x';

    switch (mark) {
    case 0:
      markChar = MARK_HEART;
      break;
    case 1:
      markChar = MARK_DIAMOND;
      break;
    case 2:
      markChar = MARK_SPADE;
      break;
    case 3:
      markChar = MARK_CLOVER;
      break;
    }

    return markChar;
  }

  public String toString() {
    return "[" + getMark() + "-" + getBlackJackPoint() + "]";
  }

}
