public class DealerPerson extends Person {

  /**
  * 手札が17枚以上になるまでカードを引き続ける
  */
  public void CardAutoDraw(Deck deck) {
    while (getHandsSum() < 17) {
      AddCard(deck.Draw());
      System.out.println("Dealerが1枚ドローしました。");
    }
  }

}
